class ApplicationController < ActionController::Base

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :session_cart

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception



  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
  end

  def logged_in_user
    unless user_signed_in?
      flash[:danger] = 'Please log in'
      redirect_to new_user_session_path
    end
  end

  def access_denied(exception)
    flash[:danger] = exception.message
    redirect_to root_url
  end

  def session_cart
    if session[:order].nil? or session[:order].empty?
      session[:order] = []
    end
  end

end
