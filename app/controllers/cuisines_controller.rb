class CuisinesController < ApplicationController

  def index
    @cuisines = Cuisine.all

  end

  def show
    @cuisine = Cuisine.find(params[:id])
    @food = Food.all
  end

  def add
    @cuisine = Cuisine.find(params[:id])
    :session_cart

    params[:cart].each do |key, value|
      if value.to_i >= 1
        session[:order]<< {key => value.to_i}
      end
    end

    redirect_to cuisines_show_path(@cuisine)
  end

  def clear
    session[:order] = []
    redirect_to root_path
  end
end
