ActiveAdmin.register Cuisine do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  permit_params :title, :description, :image

  form do |f|
    f.inputs do
      f.input :title
      f.input :description
      f.input :image, :as => :file, :hint => image_tag(f.object.image.url(:thumb))
    end
    f.actions
  end


  index do
    selectable_column
    id_column
    column :image do |cuisine|
      image_tag cuisine.image.url(:thumb)
    end
    column :title do |cuisine|
      link_to cuisine.title, admin_cuisine_path(cuisine)
    end
    column :description
    actions
  end


end
