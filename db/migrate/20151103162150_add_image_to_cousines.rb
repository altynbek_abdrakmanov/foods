class AddImageToCousines < ActiveRecord::Migration
  def change
    remove_column :cuisines, :icon

    add_attachment :cuisines, :image
  end
end
