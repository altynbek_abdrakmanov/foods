class AddCuisineToFoods < ActiveRecord::Migration
  def change
    add_reference :foods, :cuisine, index: true, foreign_key: true
  end
end
