# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

fixtures_path = Rails.root.join('app', 'assets', 'images', 'fixtures')

arzu = Cuisine.create(title: 'Arzu', image: File.new(fixtures_path.join('arzu.png')), description: Faker::Lorem.paragraph)
supara = Cuisine.create(title: 'Supara', image: File.new(fixtures_path.join('supara.png')), description: Faker::Lorem.paragraph)
diar = Cuisine.create(title: 'Diar', image: File.new(fixtures_path.join('diar.png')), description: Faker::Lorem.paragraph)
fakir = Cuisine.create(title: 'Fakir', image: File.new(fixtures_path.join('fakir.png')), description: Faker::Lorem.paragraph)
buhara = Cuisine.create(title: 'Buhara', image: File.new(fixtures_path.join('buhara.png')), description: Faker::Lorem.paragraph)


Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: arzu)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: supara)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: supara)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: diar)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: diar)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: fakir)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: fakir)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: buhara)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: buhara)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: arzu)
Food.create(title: Faker::Lorem.word, price: Faker::Number.between(100, 200),description: Faker::Lorem.paragraph, cuisine: arzu)


admin = User.create(name: 'Admin', email: 'admin@example.com', password: '123456789', password_confirmation: '123456789', admin: true)
